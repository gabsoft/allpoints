
// ScrollDown header logic
const NAV_WRAPPER = document.querySelector(".nav-wrapper");
const INFO_WRAPPER = document.querySelector(".info-wrapper")
const INPUT_WRAPPER = document.querySelector(".input-wrapper")
window.addEventListener('scroll', () => {
  if (window.pageYOffset > 100) {
    NAV_WRAPPER.classList = 'nav-small-wrapper'
    INFO_WRAPPER.classList = 'displaynone'
    INPUT_WRAPPER.classList += 'display'
  } else {
    NAV_WRAPPER.classList = 'nav-wrapper'
    INFO_WRAPPER.classList = 'info-wrapper'
    INPUT_WRAPPER.classList = 'displaynone'
  }
})

// Counter menu logic
document.querySelectorAll("#button").forEach(map => {
  map.addEventListener('click', () => {
    incrementCounter(map)
  })
})

// Dropdown menu logic
document.querySelectorAll(".filter-menu").forEach(map => {
  map.addEventListener('click', (e) => {
    e.preventDefault()
    document.querySelectorAll('.dropdown-wrapper').forEach(val => {
      val.classList.remove('display')
    })
    map.nextElementSibling.classList.add('display')
  })
})

// Clean/Apply filter options
document.querySelectorAll(".dropdown-flex li a").forEach(map => {
  map.addEventListener('click', (e) => {
    e.preventDefault()
    applyFilter(map)
  })
})

// jQuery for Calendar purpose only
$('input[name="datefilter"]').daterangepicker({
  autoUpdateInput: false,
  locale: {
    cancelLabel: 'Clear'
  }
});
$('input[name="datefilter"]').on('apply.daterangepicker', function (ev, picker) {
  $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
});
$('input[name="datefilter"]').on('cancel.daterangepicker', function (ev, picker) {
  $(this).val('');
});


function incrementCounter(val) {
  let newVal
  let oldValue
  if (val.value === "+") {
    oldValue = val.previousElementSibling.value
    newVal = parseFloat(oldValue) + 1
    val.previousElementSibling.value = newVal
  } else {
    decrementCounters(val)
  }

  function decrementCounters(val) {
    oldValue = val.nextElementSibling.value
    if (oldValue > 0) {
      newVal = parseFloat(oldValue - 1)
      val.nextElementSibling.value = newVal
    } else {
      newVal = 0
      val.nextElementSibling.value = newVal
    }
  }
}


function applyFilter(val) {
  let DROPDOWN_WRAPPER = val.parentNode.parentNode.parentNode.parentNode
  if (val.textContent === "Aplicar") {
    var counterValues = []
    DROPDOWN_WRAPPER.classList.remove('display')
    DROPDOWN_WRAPPER.querySelectorAll(".checkbox").forEach(map => {
      if (map.checked === true) {
        counterValues.push(map.checked)
      }
    })
    DROPDOWN_WRAPPER.querySelectorAll(".counter").forEach(map => {
      if (map.value > 0) {
        counterValues.push(map.value)
      }
    })
    DROPDOWN_WRAPPER.querySelectorAll('input[name="datefilter"]').forEach(map => {
      if (map.value.length > 0) {
        counterValues.push(map.value)
      }
    })

    if (counterValues.length > 0) {
      DROPDOWN_WRAPPER.parentNode.children[0].classList.add('purpleFilterSelected')
    } else {
      DROPDOWN_WRAPPER.parentNode.children[0].classList.remove('purpleFilterSelected')
    }

  } else {
    cleanFilter(DROPDOWN_WRAPPER)
  }
}

function cleanFilter(selectedClass) {
  selectedClass.querySelectorAll(".counter").forEach(map => {
    map.value = 0
  })
  selectedClass.querySelectorAll(".checkbox").forEach(map => {
    map.checked = false
  })
  selectedClass.querySelectorAll('input[name="datefilter"]').forEach(map => {
    map.value = ""
  })
}




